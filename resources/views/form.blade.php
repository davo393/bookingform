<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Booking Form</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- JQuery UI -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body>
<div id="booking" class="section">
    <div class="section-center">
        <div class="container">
            <div class="row">
                @if (\Session::has('msg'))
                    <div class="alert alert-{!! \Session::get('msg_type') !!}">
                        <ul>
                            <li>{!! \Session::get('msg') !!}</li>
                        </ul>
                    </div>
                @endif
                <div class="booking-form">
                    <div class="booking-bg"></div>
                    <form method="post" id="booking_form" action="/book" >
                        @csrf
                        <div class="form-header">
                            <h2>Fast Booking</h2>
                        </div>
                        <div class="form-group">
                            <span class="form-label">From*</span>
                            <div class="input-group">
                                <input class="form-control datepicker" name="from" id="from" value="{{old('from')}}">
                                <div class="input-group-addon" style="cursor: pointer;" onclick="$('#from').focus();">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                            @if ($errors->has('from'))
                                <span class="text-danger">{{ $errors->first('from') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="form-label">To*</span>
                            <div class="input-group">
                                <input class="form-control datepicker" name="to" id="to" value="{{old('to')}}">
                                <div class="input-group-addon" style="cursor: pointer;" onclick="$('#to').focus();">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                            @if ($errors->has('to'))
                                <span class="text-danger">{{ $errors->first('to') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="form-label">Name*</span>
                            <input class="form-control" type="text" name="name" placeholder="Enter your name" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group" id="telephone_group">
                            <span class="form-label">Phone*</span>
                            <input class="form-control" type="tel" name="telephone" id="telephone" placeholder="Enter your phone number" value="{{old('telephone')}}">
                            @if ($errors->has('telephone'))
                                <span class="text-danger">{{ $errors->first('telephone') }}</span>
                            @endif
                        </div>
                        <div class="form-btn">
                            <button type="button" onclick="validate()"  class="submit-btn">Book Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $( function() {
        $( ".datepicker" ).datepicker({
            'dateFormat': 'dd.mm.yy'});
    } );

    function validate()
    {
        $('#wrong').remove();
        var phoneNumber = document.getElementById('telephone').value;
        var phoneRGEX = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
        var phoneResult = phoneRGEX.test(phoneNumber);
        if(phoneResult) {
            document.getElementById("booking_form").submit();
            return;
        }
        $('#telephone_group').append('<span id="wrong" class="text-danger">Wrong format!</span>');
    }

</script>
</html>