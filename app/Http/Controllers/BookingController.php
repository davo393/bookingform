<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;

class BookingController extends Controller
{
    private $api_url = 'https://ko.tour-shop.ru/siteLead';

    public function __invoke(BookingRequest $request)
    {
        $msg_type = 'danger';

        $data = [
            'site_id' => env('KO_SITE_ID'),
            'type'    => 'order',
            'data'    => [
                ['name' => 'Дата заезда', 'value' => $request->from],
                ['name' => 'Дата выезда', 'value' => $request->to],
                ['name' => 'Имя', 'value' => $request->name],
                ['name' => 'Телефон', 'value' => $request->telephone],
            ]
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "KoSiteKey: ".env('KO_SITE_KEY'),
        ));

        $output = curl_exec($ch);

        if(curl_errno($ch)){
            $response = 'Something went wrong.';
            return back()->with(['msg' => $response, 'msg_type' => $msg_type]);
        }
        curl_close($ch);

        switch ($output) {
            case 'error:403':
                $response = 'Wrong permission.';
                break;
            case 'error:data':
                $response = 'Something went wrong.';
                break;
            case 'error:data-value':
                $response = 'The provided data is incorrect.';
                break;
            default:
                $booking_id = filter_var($output, FILTER_SANITIZE_NUMBER_INT);
                $response = 'Success! Book ID: '.$booking_id;
                $msg_type = 'success';
        }

        return back()->with(['msg' => $response, 'msg_type' => $msg_type]);
    }
}
